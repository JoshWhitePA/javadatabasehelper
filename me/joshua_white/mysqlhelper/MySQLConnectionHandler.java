package me.joshua_white.mysqlhelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;

/**
 * The Class MySQLImplementation.
 *
 * @author Josh
 */


/**
 * 
 * Example
 * //		MySQLConnectionHandler imp = new MySQLConnectionHandler("192.168.0.6","bob","1138","Organization");
//		String query = "select * from Employees where id = ?";
//		ArrayList<Object> paramVals = new ArrayList<Object>();
//		
//		paramVals.add(new Integer(111));
//		ResultSet mySQLResults = imp.runQuery(query, paramVals);
//		
//		ArrayList<String> coulmnToRead = new ArrayList<String>();
//		coulmnToRead.add("id");
//		coulmnToRead.add("first");
//		coulmnToRead.add("last");
//		imp.readResults(mySQLResults, coulmnToRead);
//		
//		String createStatement = "create table Animal (Name varchar(100) primary key, Color varchar(100), Roar varchar(100))";
//		System.out.println(imp.runCreateTable(createStatement));
////		String insertQuery = "insert into Employees (id,age,first,last) values(?,?,?,?)";
////		ArrayList<Object>insertVals = new ArrayList<Object>();
////		insertVals.add(new Integer(111));
////		insertVals.add(new Integer(22));
////		insertVals.add(new String("Josh"));
////		insertVals.add(new String("White"));
////		System.out.println(imp.runInsert(insertQuery, insertVals) + "");
//		
//		
 *
 */
class MySQLConnectionHandler {
	  
  	/** The conn. */
  	private Connection conn = null;
  	
	/**
 * Instantiates a new my sql implementation.
 *
 * @param host the host
 * @param user the user
 * @param passwd the passwd
 * @param databaseName the database name
 * @throws Exception the exception
 */
public MySQLConnectionHandler(String host,String user,String passwd,String databaseName) throws Exception{
		Class.forName("com.mysql.jdbc.Driver");
	      // Setup the connection with the DB
		conn = DriverManager.getConnection("jdbc:mysql://" + host + "/"+databaseName+"?" + "user=" + user + "&password=" + passwd );
	}
	

	/**
	 * Builds the query.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return the prepared statement
	 * @throws SQLException the SQL exception
	 */
	private PreparedStatement buildQuery(String query, ArrayList<Object> paramVals) throws SQLException{
		PreparedStatement preparedStatement = conn.prepareStatement(query);
		for (int idx = 0; idx < paramVals.size(); idx++){
			if(Integer.class.isInstance(paramVals.get(idx))){
				preparedStatement = furnishVal(preparedStatement,(Integer)paramVals.get(idx),idx);
			}else if(Double.class.isInstance(paramVals.get(idx))){
				preparedStatement = furnishVal(preparedStatement,(Double)paramVals.get(idx),idx);
			}else if(Byte.class.isInstance(paramVals.get(idx))){
				preparedStatement = furnishVal(preparedStatement,(Byte)paramVals.get(idx),idx);
			}else if(Float.class.isInstance(paramVals.get(idx))){
				preparedStatement = furnishVal(preparedStatement,(Float)paramVals.get(idx),idx);
			}else if(Date.class.isInstance(paramVals.get(idx))){
				preparedStatement = furnishVal(preparedStatement,(Date)paramVals.get(idx),idx);
			}else if(String.class.isInstance(paramVals.get(idx))){
				preparedStatement = furnishVal(preparedStatement,(String)paramVals.get(idx),idx);
			}
		}
		return preparedStatement;
	}
	
	/**
	 * Run query.
	 *
	 * @param query the query
	 * @param paramVals the parameter values
	 * @return the result set
	 * @throws SQLException the SQL exception
	 */
	public ResultSet runQuery(String query, ArrayList<Object> paramVals) throws SQLException {
		PreparedStatement prepState = buildQuery(query, paramVals);
		ResultSet resultSet = prepState.executeQuery();
		
		return resultSet;
	}
	
	
	/**
	 * Runs query without any parameters.
	 *
	 * @param query the query
	 * @return the result set
	 * @throws SQLException the SQL exception
	 */
	public ResultSet runQuery(String query) throws SQLException {
		ResultSet resultSet = conn.prepareStatement(query).executeQuery();
		return resultSet;
	}
	
	
	/**
	 * Run insert.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public boolean runInsert(String query, ArrayList<Object> paramVals) throws SQLException {
		PreparedStatement prepState = buildQuery(query, paramVals);
		if(prepState.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * Run insert.
	 *
	 * @param query the query
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public boolean runInsert(String query) throws SQLException {
		if(conn.prepareStatement(query).executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * Run update.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int runUpdate(String query, ArrayList<Object> paramVals) throws SQLException {
		int rowsAffected;
		PreparedStatement prepState = buildQuery(query, paramVals);
		rowsAffected = prepState.executeUpdate();
		return rowsAffected;
	}
	
	/**
	 * Run update.
	 *
	 * @param query the query
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int runUpdate(String query) throws SQLException {
		int rowsAffected;
		rowsAffected = conn.prepareStatement(query).executeUpdate();
		return rowsAffected;
	}
	
	/**
	 * Run delete.
	 *
	 * @param query the query
	 * @param paramVals the param vals
	 * @return the int
	 * @throws SQLException the SQL exception
	 */
	public int runDelete(String query, ArrayList<Object> paramVals) throws SQLException {
		int rowsAffected;
		PreparedStatement prepState = buildQuery(query, paramVals);
		rowsAffected = prepState.executeUpdate();
		return rowsAffected;
	}
	
	/**
	 * Run delete.
	 *
	 * @param query the query
	 * @return rowsAffected the number of rows affected
	 * @throws SQLException the SQL exception
	 */
	public int runDelete(String query) throws SQLException {
		int rowsAffected;
		rowsAffected = conn.prepareStatement(query).executeUpdate();
		return rowsAffected;
	}
	
	 /**
 	 * Run create table.
 	 *
 	 * @param createStatement the create statement
 	 * @return true, if successful
 	 * @throws SQLException the SQL exception
 	 */
 	public boolean runCreateTable(String createStatement) throws SQLException {
		 return conn.prepareStatement(createStatement).execute();
	 }
	
 	/**
 	 * Read Results.
 	 *
 	 * @param resultSet the result set
 	 * @param key the key
 	 * @throws SQLException the SQL exception
 	 */
 	public void readResults(ResultSet resultSet, ArrayList<String> key ) throws SQLException {
		    // ResultSet is initially before the first data set
 			ArrayList<String> valuesByTag = new ArrayList<String>();
		    while (resultSet.next()) {
		    	for(int idx = 0;idx < key.size(); idx++){
		    		String value = resultSet.getString(key.get(idx));
		    		valuesByTag.add("Value of "+key.get(idx)+": "+value);
		    	}
		    }
		    for(String output:valuesByTag){
		    	System.out.println(output);
		    }
		  }
	 
	 /**
 	 * Display results.
 	 *
 	 * @param columnKey the column key
 	 * @param columnValue the column value
 	 */
 	
 	private void displayResults(String columnKey, String columnValue){
		 System.out.println(columnKey +": " + columnValue);
	 }
	private PreparedStatement furnishVal(PreparedStatement preparedStatement,Byte objFromList, int indexOfParam) throws SQLException{
		preparedStatement.setByte(indexOfParam+1, (Byte) objFromList);
		return preparedStatement;
	}
	private PreparedStatement furnishVal(PreparedStatement preparedStatement,String objFromList, int indexOfParam) throws SQLException{
		preparedStatement.setString(indexOfParam+1, (String) objFromList);
		return preparedStatement;
	}
	private PreparedStatement furnishVal(PreparedStatement preparedStatement,Integer objFromList, int indexOfParam) throws SQLException{
		preparedStatement.setInt(indexOfParam+1, (Integer) objFromList);
		return preparedStatement;
	}
	private PreparedStatement furnishVal(PreparedStatement preparedStatement,Double objFromList, int indexOfParam) throws SQLException{
		preparedStatement.setDouble(indexOfParam+1, (Double) objFromList);
		return preparedStatement;
	}
	private PreparedStatement furnishVal(PreparedStatement preparedStatement,Float objFromList, int indexOfParam) throws SQLException{
		preparedStatement.setFloat(indexOfParam+1, (Float) objFromList);
		return preparedStatement;
	}
	private PreparedStatement furnishVal(PreparedStatement preparedStatement,Date objFromList, int indexOfParam) throws SQLException{
		preparedStatement.setDate(indexOfParam+1, (Date) objFromList);
		return preparedStatement;
	}
	
}

